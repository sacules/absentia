package main

import (
	"flag"
	"os"

	"gitlab.com/sacules/log"

	"gitlab.com/sacules/absentia/audio"
	"gitlab.com/sacules/absentia/database"
)

const dbName = "tracks.db"

func main() {
	flag.BoolVar(&log.EnableDebug, "debug", false, "enable debug logging")
	flag.Parse()

	db, err := database.Open(dbName)
	if err != nil {
		log.Error("couldn't load database:", err)
		os.Exit(1)
	}
	defer db.Close()
	p := audio.NewPlayer()

	var tracks []*audio.Track
	q := &queue{tracks}
	if len(os.Args) > 1 {
		for _, songname := range flag.Args() {
			t, err := audio.NewTrack(songname)
			if err != nil {
				log.Warning("couldn't load track", songname+":", err)
				continue
			}

			tracks = append(tracks, t)
		}

		q.tracks = tracks

		if len(tracks) > 0 {
			p.Paused = false
			err = p.Play(q.pop())
			if err != nil {
				log.Error(err)
				os.Exit(1)
			}
		}
	}

	s := state{player: p, db: db, tracks: tracks, queue: q}

	tui := newTui(s)
	tui.init()
	tui.update()
	tui.run()
}
