package audio

import (
	"os"
	"path/filepath"

	"github.com/dhowden/tag"
	"gitlab.com/sacules/log"
)

// Track contains both the audio source and the metadata
type Track struct {
	Path     string `storm:"id,increment"`
	Source   *os.File
	Metadata tag.Metadata `storm:"index"`
}

// NewTrack returns a new Track from the given file. Must be an absolute path.
func NewTrack(filename string) (*Track, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	meta, err := tag.ReadFrom(f)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return &Track{Path: filename, Source: f, Metadata: meta}, nil
}

// TracksFromDir reads all the files in a given dir
func TracksFromDir(dirname string) ([]*Track, error) {
	tracks := make([]*Track, 0)
	err := filepath.Walk(dirname, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Error("walking dir:", err)
			return err
		}

		if info.IsDir() {
			return nil
		}

		t, err := NewTrack(path)
		if err != nil {
			log.Error("reading tags:", err)
			return err
		}

		tracks = append(tracks, t)
		return nil
	})

	if err != nil {
		log.Error("new tracks from dir:", err)
		return nil, err
	}

	return tracks, nil
}
