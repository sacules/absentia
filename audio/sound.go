package audio

import (
	"fmt"
	"os"
	"time"

	"github.com/dhowden/tag"
	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/flac"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/vorbis"
)

// Player allows to control the current audio stream
type Player struct {
	Paused bool

	// Length is the amount of frames of the current song
	Length int

	// Duration is the total track duration
	Duration time.Duration

	CurrentTrack *Track

	ctrl     *beep.Ctrl
	format   beep.Format
	vol      *effects.Volume
	streamer beep.StreamSeeker
}

func NewPlayer() Player {
	return Player{Paused: true}
}

// VolumeChange modifies the audio stream's volume.
func (p *Player) VolumeChange(n float64) {
	speaker.Lock()
	p.vol.Volume = n
	speaker.Unlock()
}

// Play starts the playback of the audio stream
func (p *Player) Play(t *Track) error {
	p.CurrentTrack = t
	err := p.read()
	if err != nil {
		return err
	}

	speaker.Play(p.vol)

	return p.ctrl.Err()
}

// Pause stops the playback of the audio stream
func (p *Player) Pause() error {
	speaker.Lock()
	defer speaker.Unlock()

	p.ctrl.Paused = !p.ctrl.Paused
	p.Paused = p.ctrl.Paused

	return p.ctrl.Err()
}

func (p *Player) MoveSecondBackward() error {
	newpos := p.CurrentPosition()
	newpos -= p.SamplesPerSecond() * 5
	return p.MovePlayback(newpos)
}

func (p *Player) MoveSecondForward() error {
	newpos := p.CurrentPosition()
	newpos += p.SamplesPerSecond() * 5
	return p.MovePlayback(newpos)
}

// MovePlayback moves the player to sample n
func (p *Player) MovePlayback(n int) error {
	speaker.Lock()
	defer speaker.Unlock()

	newpos := n

	if n < 0 {
		newpos = 0
	}

	if n >= p.Length {
		newpos = p.Length - 1
	}

	return p.streamer.Seek(newpos)
}

// SamplesPerSecond returns the amount of samples in 1 second
func (p *Player) SamplesPerSecond() int {
	return p.format.SampleRate.N(time.Second)
}

// CurrentDuration returns in how far the track is playing
func (p *Player) CurrentDuration() string {
	dur := p.format.SampleRate.D(p.streamer.Position()).Round(time.Second)
	return fmt.Sprintf("%02d:%02d / %02d:%02d",
		int(dur.Minutes()),
		int(dur.Seconds())%60,
		int(p.Duration.Minutes()),
		int(p.Duration.Seconds())%60)
}

// CurrentPosition returns the current frame
func (p *Player) CurrentPosition() int {
	return p.streamer.Position()
}

func (p *Player) read() error {
	var (
		streamer beep.StreamSeekCloser
		format   beep.Format
		err      error
	)

	f, err := os.Open(p.CurrentTrack.Path)
	if err != nil {
		return err
	}

	p.CurrentTrack.Source = f

	switch p.CurrentTrack.Metadata.FileType() {
	case tag.MP3:
		streamer, format, err = mp3.Decode(p.CurrentTrack.Source)

	case tag.FLAC:
		streamer, format, err = flac.Decode(p.CurrentTrack.Source)

	case tag.OGG:
		streamer, format, err = vorbis.Decode(p.CurrentTrack.Source)

	default:
		return fmt.Errorf("unknown format")
	}
	if err != nil {
		return fmt.Errorf("reading: %v", err)
	}

	speaker.Clear()
	err = speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/30))
	if err != nil {
		return fmt.Errorf("couldn't init speakers")
	}

	ctrl := &beep.Ctrl{
		Streamer: beep.Loop(1, streamer),
		Paused:   p.Paused,
	}

	resampler := beep.ResampleRatio(4, 1, ctrl)
	vol := &effects.Volume{
		Streamer: resampler,
		Base:     2,
	}

	p.streamer = streamer
	p.format = format
	p.ctrl = ctrl
	p.vol = vol
	p.Length = streamer.Len()
	p.Duration = format.SampleRate.D(streamer.Len()).Round(time.Second)

	return nil
}
