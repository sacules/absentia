# Absentia | A music player for lunatics
[![Go Doc](https://img.shields.io/badge/godoc-reference-blue.svg?style=flat-square)](https://godoc.org/gitlab.com/Sacules/absentia)

This package has the functions needed in order to build a music player, which
can be found in the `cmd` dir.

## Prerequisites
You'll need Go 1.11 or higher, a C compiler, and some libraries.

### Linux
On Ubuntu or Debian, run:

```console
sudo apt install libasound2-dev libegl1-mesa-dev xorg-dev
```

## Building
You can then install it simply with:

```console
go get gitlab.com/sacules/absentia/cmd/absentia
```

## Usage
Right now it only supports mp3 files.
```console
$ absentia [music file]
```
