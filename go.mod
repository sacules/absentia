module gitlab.com/sacules/absentia

go 1.11

require (
	fyne.io/fyne v1.2.4
	github.com/asdine/storm v2.1.2+incompatible
	github.com/dhowden/tag v0.0.0-20200412032933-5d76b8eaae27
	github.com/faiface/beep v1.0.2
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gdamore/tcell v1.3.0
	github.com/go-gl/glfw v0.0.0-20200222043503-6f7a984d4dc4 // indirect
	github.com/hajimehoshi/go-mp3 v0.2.1 // indirect
	github.com/hajimehoshi/oto v0.5.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/tview v0.0.0-20200818120338-53d50e499bf9
	github.com/srwiley/rasterx v0.0.0-20200120212402-85cb7272f5e9 // indirect
	gitlab.com/sacules/log v0.1.0
	go.etcd.io/bbolt v1.3.5 // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
	golang.org/x/mobile v0.0.0-20200329125638-4c31acba0007 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/text v0.3.2 // indirect
)
