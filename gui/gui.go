package gui

import (
	"fmt"

	"fyne.io/fyne"
	"fyne.io/fyne/app"

	"gitlab.com/sacules/absentia/audio"
)

type GUI struct {
	app    fyne.App
	win    fyne.Window
	player *player
	lib    *library
}

func New() *GUI {
	p := newPlayer()
	p.setup()

	app := app.New()
	w := app.NewWindow("Absentia")

	return &GUI{
		app:    app,
		win:    w,
		player: p,
	}
}

func (g *GUI) Run(ap audio.Player) error {
	g.win.SetContent(g.player.box)
	g.win.SetFixedSize(true)

	err := g.player.update(ap)
	if err != nil {
		return fmt.Errorf("gui: %v", err)
	}

	g.win.ShowAndRun()

	return nil
}
