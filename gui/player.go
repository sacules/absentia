package gui

import (
	"fmt"
	"time"

	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"

	"gitlab.com/sacules/absentia/audio"
	"gitlab.com/sacules/log"
)

type player struct {
	cover        *canvas.Image
	title        *widget.Label
	artist       *widget.Label
	progressText *widget.Label
	progressBar  *widget.Slider
	buttonPlay   *widget.Button
	volumeBar    *widget.Slider
	volumeLabel  *widget.Label

	box *widget.Box
}

func newPlayer() *player {
	p := &player{
		title:        widget.NewLabel(""),
		artist:       widget.NewLabel(""),
		progressText: widget.NewLabel(""),
		progressBar:  widget.NewSlider(0, 0),
		buttonPlay:   widget.NewButtonWithIcon("", theme.MediaPauseIcon(), nil),
		volumeBar:    widget.NewSlider(-10, 0),
		volumeLabel:  widget.NewLabel("Volume"),
	}

	p.box = widget.NewVBox(
		p.title, p.artist, p.progressText,
		p.progressBar, p.buttonPlay, p.volumeBar,
	)

	return p
}

// configure the visuals
func (p *player) setup() {
	p.title.Alignment = fyne.TextAlignCenter
	p.title.TextStyle.Bold = true
	p.artist.Alignment = fyne.TextAlignCenter
	p.progressText.Alignment = fyne.TextAlignCenter
	p.progressBar.Min = 0
	p.volumeBar.Step = 0.1
}

// called on each song change
func (p *player) update(ap audio.Player) error {
	p.title.SetText(ap.CurrentTrack.Metadata.Title())
	p.artist.SetText(ap.CurrentTrack.Metadata.Artist())

	p.progressBar.Max = float64(ap.Length)
	p.progressBar.Step = float64(ap.SamplesPerSecond())
	// TODO: replace with a linear volume slider
	p.progressBar.OnChanged = func(n float64) {
		log.Debugf("moving playback: frame=%.0f duration=%s", n, ap.CurrentDuration())
		err := ap.MovePlayback(int(n))
		if err != nil {
			log.Error("progress bar:", err)
			return
		}

		p.progressText.SetText(ap.CurrentDuration())
	}

	// Update playback status
	seconds := time.Tick(time.Second)
	go func() {
		for {
			// awful, is there a simpler way?
			p.progressText.SetText(ap.CurrentDuration())

			p.progressBar.Value = float64(ap.CurrentPosition())
			if p.progressBar.Value >= float64(ap.Length) {
				p.progressBar.Value = 0
			}

			p.progressText.Refresh()
			p.progressBar.Refresh()

			<-seconds
		}
	}()

	p.buttonPlay.OnTapped = func() {
		err := ap.Pause()
		if err != nil {
			log.Error(err)
			panic(err)
		}

		if ap.Paused {
			p.buttonPlay.SetIcon(theme.MediaPlayIcon())
		} else {
			p.buttonPlay.SetIcon(theme.MediaPauseIcon())
		}
	}

	p.volumeBar.OnChanged = func(n float64) {
		log.Debug("changing volume by", fmt.Sprintf("%.2f", n))
		ap.VolumeChange(n)
	}

	img, err := extractImage(ap.CurrentTrack)
	if err != nil {
		log.Info(err)
		return nil
	}

	p.cover = canvas.NewImageFromImage(img)
	p.cover.SetMinSize(fyne.Size{Width: 200, Height: 200})
	p.box.Prepend(p.cover)

	return nil
}
