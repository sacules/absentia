package gui

import (
	"bytes"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"

	"gitlab.com/sacules/log"
	_ "golang.org/x/image/bmp"
	_ "golang.org/x/image/tiff"

	"gitlab.com/sacules/absentia/audio"
)

func extractImage(t *audio.Track) (image.Image, error) {
	pic := t.Metadata.Picture()
	if pic == nil {
		return nil, fmt.Errorf("no artwork found")
	}

	log.Debug("found artwork:", pic)

	r := bytes.NewReader(pic.Data)
	img, fmt, err := image.Decode(r)
	if err != nil {
		return nil, err
	}
	log.Debug("extracted artwork in format", fmt)

	return img, nil
}
