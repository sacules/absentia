// Package db provides the interface for the inner database
package database

import (
	"fmt"

	"github.com/asdine/storm"

	"gitlab.com/sacules/absentia/audio"
)

type Database struct {
	db *storm.DB
}

func Open(name string) (*Database, error) {
	db, err := storm.Open(name)
	if err != nil {
		return nil, fmt.Errorf("setup database: %v", err)
	}

	return &Database{db}, nil
}

func (d *Database) Load() ([]*audio.Track, error) {
	var tracks []*audio.Track
	err := d.db.All(&tracks)
	if err != nil {
		return nil, fmt.Errorf("loading tracks: %v", err)
	}

	return tracks, nil
}

func (d *Database) Save(tracks []*audio.Track) error {
	for _, t := range tracks {
		err := d.db.Save(t)
		if err != nil {
			return err
		}
	}

	return nil
}

func (d *Database) Close() error {
	return d.db.Close()
}
