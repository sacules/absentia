package main

import (
	"gitlab.com/sacules/absentia/audio"
)

type queue struct {
	tracks []*audio.Track
}

func (q *queue) push(t *audio.Track) {
	q.tracks = append(q.tracks, t)
}

func (q *queue) pop() *audio.Track {
	if len(q.tracks) == 0 {
		return nil
	}

	t := q.tracks[0]
	q.tracks = q.tracks[1:]

	return t
}

func (q *queue) clear() {
	q.tracks = make([]*audio.Track, 0)
}
