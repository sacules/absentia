package main

import (
	"fmt"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/sacules/absentia/audio"
	"gitlab.com/sacules/absentia/database"
	"gitlab.com/sacules/log"
)

type state struct {
	db     *database.Database
	tracks []*audio.Track
	queue  *queue
	player audio.Player
}

type tui struct {
	state state

	app       *tview.Application
	mainview  *tview.Flex
	current   *tview.TextView
	tracklist *tview.Table
}

func newTui(s state) *tui {
	return &tui{
		state:     s,
		app:       tview.NewApplication(),
		mainview:  tview.NewFlex(),
		current:   tview.NewTextView(),
		tracklist: tview.NewTable(),
	}
}

func (t *tui) init() {
	go t.changeSongOnFinish()

	// inputs
	t.app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Rune() {
		case 'a':
			row, _ := t.tracklist.GetSelection()
			track := t.state.tracks[row]
			t.state.queue.push(track)

		case 'q':
			err := t.state.player.Pause()
			if err != nil {
				log.Error(err)
			}

			t.app.Stop()
			err = t.state.db.Save(t.state.tracks)
			if err != nil {
				log.Error("saving tracks:", err)
			}

		case 'h':
			err := t.state.player.MoveSecondBackward()
			if err != nil {
				log.Warning(err)
			}

		case 'l':
			err := t.state.player.MoveSecondForward()
			if err != nil {
				log.Warning(err)
			}

		case ' ':
			t.state.player.Pause()
			t.update()
		}

		switch event.Key() {
		case tcell.KeyCtrlSpace:
			err := t.state.player.Pause()
			if err != nil {
				log.Error(err)
			}

			t.update()
		}

		return event
	})

	t.tracklist.SetSelectedFunc(func(row, column int) {
		t.state.queue.clear()
		track := t.state.tracks[row]
		t.state.player.Play(track)

		go t.app.QueueUpdateDraw(func() {
			t.current.Clear()
			t.update()
		})
	})

	// primitives
	t.tracklist.SetSelectable(true, false)
	t.current.SetDynamicColors(true)

	t.mainview.SetDirection(tview.FlexRow)
	t.mainview.AddItem(t.tracklist, 0, 1, true)
	t.mainview.AddItem(t.current, 3, 0, false)

	t.app.SetFocus(t.mainview).SetRoot(t.mainview, true)
}

func (t *tui) update() {
	t.current.Clear()
	t.tracklist.Clear()

	for i, song := range t.state.tracks {
		art := fmt.Sprintf("[::b]%s[::-]", song.Metadata.Artist())
		t.tracklist.SetCellSimple(i, 0, art)
		t.tracklist.SetCellSimple(i, 1, song.Metadata.Title())
	}

	if t.state.player.CurrentTrack != nil {
		icon := "⏵︎"
		if t.state.player.Paused {
			icon = "⏸︎"
		}

		meta := t.state.player.CurrentTrack.Metadata
		fmt.Fprintln(t.current)
		fmt.Fprintf(t.current, "[::b]%s  %s[::-] %s", icon, meta.Artist(), meta.Title())
	}
}

func (t *tui) run() error {
	return t.app.Run()
}

func (t *tui) changeSongOnFinish() {
	ticker := time.Tick(time.Second / 10)

loop:
	for {
		select {
		case <-ticker:
			if t.state.player.CurrentPosition() != t.state.player.Length {
				continue
			}

			track := t.state.queue.pop()
			if track == nil {
				break loop
			}

			err := t.state.player.Play(track)
			if err != nil {
				log.Error(err)
			}

			t.app.QueueUpdateDraw(func() {
				t.update()
			})
		}
	}
}
